﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;
	public float force = 10f;
	Vector3 direction;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		rigidbody.useGravity = false;
		//movePositionExample ();
		//velocityExample ();
		addForceExample ();
	}

	private Vector3 GetMousePosition() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}


	//MovePosition Example

	//
	void movePositionExample () {
		Vector3 pos = GetMousePosition ();
		rigidbody.MovePosition (pos);
	}
		
//	void addForceExample () {
//		Vector3 pos = GetMousePosition ();
//		Vector3 dir = pos - rigidbody.position;
//
//		rigidbody.AddForce (dir.normalized * force);
//	}



	void addForceExample () {
		
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		rigidbody.AddForce (direction * force);
	}

	//Velocity Example
	void velocityExample () {
		Vector3 post = GetMousePosition ();
		Vector3 dir = post - rigidbody.position;
		Vector3 vel = dir.normalized * speed;
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}


	//EXPLAIN THE DIFFERENCE BETWEEN MOVEMENT USING TRANSFORM AND RIGIDBODY TO MOVE OBJECTS
	//Transform simply moves the object to a new location, like teleporting, and it'll 
	//ignore all objects with rigidbodies along the way. If another rigidbody is at the
	//new location, they'll react immediately, and strangely, to escape one another.

	//Rigidbody, however, will travel to the desginated location and will properly react
	//with all other rigidbodies along the way.

	//EXPLAIN THE DIFFERENCE BETWEEN USING MOVEPOSITION, VELOCITY, AND ADDFORCE.

	//Moveposition simply "teleports" the object rapidly to the new location. It doesn't care
	//about boundries, and when colliding with another rigidbody, it will not react properly.

	//Addforce makes the object "accelerate" towards the target, however, it will often overshoot the
	//target entirely, and then attempt to turn around. It'll infinitely be overshooting a non-moving target.

	//Velocity sends the object towards the target at a fixed rate, but will stop suddenly at the target.
	//If there are any rigidbody objects in the path, (eg. a wall), the object will be stopped.
}
